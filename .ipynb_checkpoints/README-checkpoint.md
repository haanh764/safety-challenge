# Safety - aiforsea - grab challenges

Submission for aiforsea - grab challenge - safety

## Content

- Data exploration, cleaning, and preprocessing
- Feature engineering
- Model

Note: The detailed information especially abut data features are included in notebook file.


## Structure
- Code:
  - Data preprocessing and model: safety_challenges_grab.ipynb
  - Testing model on test data: testing_model.ipynb
- Data & model:
  - Test data: X_test.csv, y_test.csv (raw data); and X_test_new.csv, y_test_new.csv (new features added)
  - Model: rf_model_base_raw.pkl, lgbm_model_base_raw.pkl
          rf_model_new_features.pkl, lgbm_rf_model_new_features.pkl


## Requirements
- Python 3.6
- Pandas, Numpy, sklearn, pickle, dask, matplotlib, seaborn